# Manuel d'utilisation : Exécutable d'automate LR(1)

## Compilation
Dans le répertoire du project, taper dans un terminal : `make`.

## Lancer l'exécutable sous Linux
Dans le répertoire où se trouve l'exécutable, taper dans un terminal `./lr1` suivi de l'emplacement d'un fichier automate (stocké dans le répertoire `aut`).

Exemple :
```console
./lr1 aut/arith.out
```

## Utiliser le programme
Taper un mot sur le terminal et valider en appuyant sur `Entrée`.

## Quitter le programme
Appuyer simultanément sur les touches `CTRL` et `D`.