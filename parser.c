#include <stdio.h>
#include <stdlib.h>
#include "parser.h"

act action(state s, char c){
    return action_table[s][c];
}

red reduce(state s){
    red res = {red1[s], red2[s]};
    return res;
}

/*  @requires t1 and t2 of size size;
    @assigns nothing
    @returns the index of first appearance of s in t1 and c in t2, where they have the same index,*/
int find_double_value_index(state s, char c, state* t1, char* t2, int size){
    int i;
    for (i=0; i<size; i+=1){
        if (t1[i] == s && t2[i] == c)
            return i;
    }
    printf("Error : Incorrect file\n");
    free_all(); exit(EXIT_FAILURE); /*erreur si aucune valeur trouvée*/
}

state shift(state s, char c){
    if (action(s,c) != 2)
        return 0; /*shift n'est pas défini pour l'état s et le caractère c*/
    int i = find_double_value_index(s, c, dec1, dec2, size_dec);
    if (i==-1){
        printf("Error : Incorrect file (shift function incorrect)\n");
        free_all(); exit(EXIT_FAILURE);
    }
    return dec3[i];
}

state branch(state s, nts A){
    int i = find_double_value_index(s, A, brc1, brc2, size_brc);
    return brc3[i];
}

void free_all(){
    free(red1);
    free(red2);
    free(dec1);
    free(dec2);
    free(dec3);
    free(brc1);
    free(brc2);
    free(brc3);
    int i;
    for (i=0;i<n;i+=1){
        free(action_table[i]);
    }
    free(action_table);
}