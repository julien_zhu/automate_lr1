#ifndef PARSER_H
#define PARSER_H

typedef char state;     /*etat sur un octet*/
typedef char act;       /*action : entre 0 et 4 -> sur un octet*/
typedef char nts;       /*symbole non-terminal*/

struct red {
    char num;           /*nombre d'états que reduce va dépiler, sur un octet*/
    nts symbol;
};

typedef struct red red;

/*Variables globales*/
act** action_table;     /*matrice des actions*/
char* red1;             /*première composante de reduit*/
nts* red2;              /*deuxième composante de reduit*/
state* dec1;            /*premier octet du groupement de trois octets dans la partie décale*/
char* dec2;             /*deuxième octet du groupement de trois octets dans la partie décale*/
state* dec3;            /*troisième octet du groupement de trois octets dans la partie décale*/
state* brc1;            /*premier octet du groupement de trois octets dans la partie branchement*/
nts* brc2;              /*deuxième octet du groupement de trois octets dans la partie branchement*/
state* brc3;            /*troisième octet du groupement de trois octets dans la partie branchement*/

int n;                  /*nombre d'états*/
int size_dec;           /*taille effective de dec1, dec2 et dec3*/
int size_brc;           /*taille effective de brc1, brc2 et brc3*/

/*  @requires c in ascii between 0 and 255
    @assigns nothing
    @ensures returns the action associated with the state s and the character c */
act action(state s, char c);


/*  @requires red1 and red2 of size > s
    @assigns nothing
    @ensures the couple of res from state s */
red reduce(state s);

/*  @requires dec1, dec2 and dec3 of size dec_size
    @assigns nothing
    @ensures the value of shift(s,c), 0 if no shift */
state shift(state s, char c);

/*  @requires brc1, brc2 and brc3 of size brc_size;
    @assigns nothing;
    @ensures the value of branch(s,A);  */
state branch(state s, nts A);

/*  @requires nothing
    @assigns the global variables : action_table, red1, red2, dec1, dec2, dec3, brc1, brc2, brc3
    @ensures free all the arrays and matrix created by mallocs for the global variables */
void free_all();

#endif