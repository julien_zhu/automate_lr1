#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

list empty_list(){
    return NULL;
}

int is_empty(list l){
    return (l==NULL);
}

/*  @requires l is a valid list
    @assigns nothing
    @ensures adds a to the top of the list l    */
list cons(value a, list l){
    list res = malloc(sizeof(list));
    res->val = a;
    res->next = l;
    return res;
}

void push(value a, list *pl){
    *pl = cons(a, *pl);
}

value pop(list *pl){
    if (*pl == NULL){
        printf("Error : Incorrect file. (Pop error : List is empty)\n");
        exit(EXIT_FAILURE);
    }
    value res;
    res = (*pl)->val;
    *pl = (*pl)->next;
    return res;
}

void print_list(list l){
    while (l != NULL){ /*Terminaison : la liste ne boucle pas car on n'utilise que les fonctions de base des piles*/
        printf("-> [%i] ", l->val);
        l = l-> next;
    }
    printf("-> []\n");
}