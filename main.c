#include <stdio.h>
#include <stdlib.h>
/*#include <signal.h>*/
#include "reading.h"

int main(int argc, char** argv){
    if (argc!=2){
        fprintf(stderr, "Usage : %s file\n", argv[0]);
        exit(1);
    }
    FILE* fd = fopen(argv[1],"rb");
    if (fd == NULL){
        fprintf(stderr, "%s : Error opening \"%s\"\n", argv[0], argv[1]);
        exit(2);
    }
    reading(fd, argv[1]);
    fclose(fd);
    /*signal(SIGINT,SIG_IGN); désactive le ctrl+c*/
    while (1){/*Terminaison : quand l'utilisateur appuie sur ctrl+D, le programme s'arrête*/
        scan();
    }
    return 0;
}