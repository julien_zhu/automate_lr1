#ifndef ARRAY_MATRIX_H
#define ARRAY_MATRIX_H

/*  @requires n>0
    @assigns nothing
    @ensures returns a char array of zeroes, of size n  */
char* create_table(int n);

/*  @requires h>0 ,w>0
    @assigns nothing
    @ensures returns an matrix of zeroes, of height h and width w   */
char** create_matrix(int h, int w);

/*  @requires t of size n;
    @assigns nothing
    @ensures prints the content of table t  */
void print_table(char t[], int n);

/*  @requires t of height h and width w;
    @assigns nothing
    @ensures prints the content of 2D table t   */
void print_matrix(char** t, int h, int w);

#endif