#ifndef PILE_H
#define PILE_H

/*Type concret des éléments de la pile (techniquement d'alias state, défini dans parser.h)*/
typedef char value;

/*Structure de liste chaînée*/
typedef struct bucket* list;

struct bucket {
    value val;
    list next;
};

/*  @requires nothing
    @assigns nothing
    @ensures returns empty list */
list empty_list();

/*  @requires l is a valid list
    @assigns nothing
    @ensures return 1 if l is empty and 0 if not    */
int is_empty(list l);

/*  @requires pl is a valid pointer to a valid list
    @assigns list
    @ensures add a to the top of the list *pl       */
void push(value a, list *pl);

/*  @requires pl is a valid pointer to a valid non empty list
    @assigns list
    @ensures returns the head of the list and removes it from the list  */
value pop(list *pl);

/*  @requires l is a valid list
    @assigns nothing
    @ensures prints the list    */
void print_list(list l);

#endif