CC=gcc -Wall

all: array_matrix.o parser.o reading.o pile.o main.o lr1

%.o: %.c %.h
	$(CC) -c $<

reading.o: parser.h array_matrix.h pile.h

main.o: main.c reading.h
	$(CC) -c $<

lr1: main.o parser.o array_matrix.o reading.o pile.o
	$(CC) $^ -o $@
	rm -f *.o