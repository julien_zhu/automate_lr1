#ifndef READING_H
#define READING_H

/*  @requires fd is a valid file descriptor, filename is the name of the fd
    @assigns action_table, red1, red2, dec1, dec2, dec3, brc1, brc2, brc3
    @ensures reads fd and puts its value to action_table, red1, red2, dec1, dec2, dec3, brc1, brc2, brc3    */
int reading(FILE* fd, char* filename);

/*  @requires filename is the name of the fd which is a valid file descriptor to a LR(1) file
    @assigns nothing
    @ensures prints the differents information of the file that has been read using global variables    */
void print_file(char* filename);

/*  @requires nothing
    @assigns nothing
    @ensures reads stdin and tests if the string is correct within LR(1) parser rules.   */
void scan();

#endif