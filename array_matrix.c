#include <stdio.h>
#include <stdlib.h>
#include "array_matrix.h"

char* create_table(int n){
    char* tab = calloc(n, sizeof(char));
    return tab;
}

char** create_matrix(int h, int w){
    char** mat = malloc(h * sizeof(char*));
    int i,j;
    for (i=0;i<h;i+=1) {
        mat[i] = calloc(w, sizeof(char));
        for (j=0;j<w;j+=1)
            mat[i][j] = 0;
    }
    return mat;
}

void print_table(char t[], int n){
    int i;
    for (i=0; i<n; i+=1)
        printf("%4i", t[i]);
    printf("\n");
}

void print_matrix(char** t, int h, int w){
    int i,j;
    for (i=0; i<h; i+=1){
        for (j=0; j<w; j+=1)
            printf("%i", t[i][j]);
        printf("\n");
    }
}