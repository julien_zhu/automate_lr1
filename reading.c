#include <stdio.h>
#include <stdlib.h>
#include "reading.h"
#include "parser.h"
#include "array_matrix.h"
#include "pile.h"

/*  @requires nb_states > 0
    @assigns nothing
    @ensures creates global variables storing the content of the LR(1) file */
void make_tables(int nb_states){
    action_table = create_matrix(nb_states,128);    /*matrice de n lignes et 128 colonnes*/
    red1 = create_table(nb_states);
    red2 = create_table(nb_states);
    dec1 = create_table(256);       /*il y a au plus 256 états donc taille maximum de 256*/
    dec2 = create_table(256);       /*il y a au plus 256 états donc taille maximum de 256*/
    dec3 = create_table(256);       /*il y a au plus 256 états donc taille maximum de 256*/
    brc1 = create_table(256);       /*il y a au plus 256 états donc taille maximum de 256*/
    brc2 = create_table(256);       /*il y a au plus 256 états donc taille maximum de 256*/
    brc3 = create_table(256);       /*il y a au plus 256 états donc taille maximum de 256*/
}

int reading(FILE* fd, char* filename){
    unsigned char c; /*éviter les valeurs ascii négatives : [0,255] au lieu de [0,127] à [-128,-1]*/
    int i,j;
    unsigned char triplet[3];
    /*Première ligne*/
    fread(&c, sizeof(char), 1, fd);
    if (c!='a'){
        fprintf(stderr, "Error : File \"%s\" is not an LR(1) descriptor\n", filename);
        exit(EXIT_FAILURE);
    }
    if (fscanf(fd, " %i\n", &n) == EOF){ /*Donne la valeur de n*/
        fprintf(stderr, "Error : File \"%s\" is not an LR(1) descriptor\n", filename);
        exit(EXIT_FAILURE);
    }
    make_tables(n);
    /*Remplissage action matrix*/
    for (i=0; i<n; i+=1){
        for (j=0;j<128;j+=1){
            fread(&c, sizeof(char), 1, fd);
            if (feof(fd)){
                fprintf(stderr, "Error reading \"%s\" : (Unexpected end of file)\n", filename);
                free_all(); exit(EXIT_FAILURE);
            }
            else if (ferror(fd)){
                fprintf(stderr, "Error reading \"%s\".\n", filename);
                free_all(); exit(EXIT_FAILURE);
            }
            else if (!(c==0 || c==1 || c==2 || c==3)){
                fprintf(stderr, "Error reading \"%s\" : Incorrect format (Actions not properly valued).\n", filename);
                free_all(); exit(EXIT_FAILURE);
            }
            action_table[i][j] = c; /*convertit le unsigned char c vers un int pour la matrice*/
        }
    }
    /*Detection saut de ligne*/
    fread(&c, sizeof(char), 1, fd);
    if (c!='\n'){
        fprintf(stderr, "Error reading \"%s\" : Incorrect format (At separation between action values and reduce values)\n", filename);
        exit(EXIT_FAILURE);
    }
    /*Remplissage tableau red (1ère composante)*/
    for (i=0;i<n;i+=1){
        fread(&c, sizeof(char), 1, fd);
        if (feof(fd)){
            fprintf(stderr, "Error reading \"%s\" : (Unexpected end of file)\n", filename);
            exit(EXIT_FAILURE);
        }
        else if (ferror(fd)){
            fprintf(stderr, "Error reading \"%s\".\n", filename);
            exit(EXIT_FAILURE);
        }
        red1[i] = c;
    }
    /*Detection saut de ligne*/
    fread(&c, sizeof(char), 1, fd);
    if (c!='\n'){
        fprintf(stderr, "Error reading \"%s\" : Incorrect format (At separation between reduce_1 values and reduce_2 values)\n", filename);
        exit(3);
    }
    /*Remplissage tableau red (1ème composante)*/
    for (i=0;i<n;i+=1){
        fread(&c, sizeof(char), 1, fd);
        if (feof(fd)){
            fprintf(stderr, "Error reading \"%s\" : (Unexpected end of file)\n", filename);
            exit(EXIT_FAILURE);
        }
        else if (ferror(fd)){
            fprintf(stderr, "Error reading \"%s\".\n", filename);
            exit(EXIT_FAILURE);
        }
        red2[i] = c;
    }
    /*Detection saut de ligne*/
    fread(&c, sizeof(char), 1, fd);
    if (c!='\n'){
        fprintf(stderr, "Error reading \"%s\" : Incorrect format (At separation between reduce_1 values and reduce_2 values)\n", filename);
        exit(EXIT_FAILURE);
    }
    /*Remplissage 3 tableaux shifts, un pour chaque octet du triplet d'octet*/
    i=0;
    do {
        fread(triplet, sizeof(char), 3, fd);
        if (feof(fd)){
            fprintf(stderr, "Error reading \"%s\" : (Unexpected end of file)\n", filename);
            exit(EXIT_FAILURE);
        }
        else if (ferror(fd)){
            fprintf(stderr, "Error reading \"%s\".\n", filename);
            exit(EXIT_FAILURE);
        }
        dec1[i] = triplet[0];
        dec2[i] = triplet[1];
        dec3[i] = triplet[2];
        i+=1;
    }
    while (triplet[0]!= 173 && triplet[1]!= 173 && triplet[2]!= 173); /*Terminaison : le format du fichier indique qu'il y a un moment où les 3 octets lus sont '\255\255\255'*/
    size_dec = i-1; /*ignore le dernier triplet qui est '\255' '\255' '\255' pour la taille de l'array*/
    /*Remplissage 3 tableaux branchs, un pour chaque octet du triplet d'octet*/
    i=0; /*réinitialisation du compteur*/
    do {
        fread(triplet, sizeof(char), 3, fd);
        if (feof(fd)){
            fprintf(stderr, "Error reading \"%s\" : (Unexpected end of file)\n", filename);
            exit(EXIT_FAILURE);
        }
        else if (ferror(fd)){
            fprintf(stderr, "Error reading \"%s\".\n", filename);
            exit(EXIT_FAILURE);
        }
        brc1[i] = triplet[0];
        brc2[i] = triplet[1];
        brc3[i] = triplet[2];
        i+=1;
    }
    while (triplet[0]!= 173 && triplet[1]!= 173 && triplet[2]!= 173); /*Terminaison : le format du fichier indique qu'il y a un moment où les 3 octets lus sont '\255\255\255'*/
    size_brc = i-1; /*ignore le dernier triplet qui est '\255' '\255' '\255' pour la taille de l'array*/
    printf("File \"%s\" correctly read.\n", filename);
    return 0;
}

void print_file(char* filename){
    printf("LR(1) File : \"%s\"\n", filename);
    printf("Number of states : n = %d\n", n);
    printf("Action values :\n");
    print_matrix(action_table, n, 128);
    printf("Reduce (1st component) values :\n");
    print_table(red1, n);
    printf("Reduce (2nd component) values :\n");
    print_table(red2, n);
    printf("Shift values 1:\n");
    print_table(dec1, size_dec);
    printf("Shift values 2:\n");
    print_table(dec2, size_dec);
    printf("Shift values 3:\n");
    print_table(dec3, size_dec);
    printf("Branch values 1:\n");
    print_table(brc1, size_brc);
    printf("Branch values 2:\n");
    print_table(brc2, size_brc);
    printf("Branch values 3:\n");
    print_table(brc3, size_brc);
}

void scan(){
    char s[256];
    printf("Please enter your inputs. (press ctrl+D to quit)\n");
    if (fgets(s, 256, stdin) == NULL/* || *s=='q'*/){ /*Détecte ctrl+D*/
        free_all();
        printf("Quitting program.\n");
        exit(EXIT_SUCCESS);
    }
    list l = empty_list();
    int i = 0;
    red r;          /*Pour case 3 = réduit*/
    int k;          /*Pour case 3 = réduit*/
    push(0, &l);    /*état initial = 0*/
    while (1){      /*Terminaison : la boucle se termine lorsque action renvoie accepte ou rejette, ce qui est toujours le cas dans l'automate*/
        switch (action(l->val, s[i])){
            case 0 : printf("Rejected (error at position %d) \n", i+1); free(l); return; break; /*Action = rejette  */
            case 1 : printf("Accepted\n"); free(l); return; break;                              /*Action = accepte  */
            case 2 : {
                push(shift(l->val, s[i]), &l);
                i+=1; /*on avance dans la chaine de caractères*/
            } break;                                                                            /*Action = décale   */
            case 3 : {                                                                          /*Action = réduit   */
                r = reduce(l->val);
                for (k=0; k<r.num; k+=1)
                    pop(&l);
                push(branch(l->val, r.symbol), &l);
            } break;
            default : fprintf(stderr,"Error scan : caracter not accepted.\n"); free(l); free_all(); exit(EXIT_FAILURE); break;
        }
    }
}